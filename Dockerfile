FROM python:3.8-buster

ENV LANG C.UTF-8

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install --upgrade pip setuptools
RUN pip3 install wheel
RUN pip3 install -r requirements.txt

RUN rm -r /root/.cache

RUN mkdir /var/log/python-tutorial

COPY . /app/
CMD gunicorn wsgi:app -w 3 -b 0.0.0.0:5080
