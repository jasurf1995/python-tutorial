from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.dialects.postgresql import UUID

from blog.application import db, login
import uuid
import datetime


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(UUID(), primary_key=True)
    email = db.Column(db.String(256), unique=True)
    password_hash = db.Column(db.String(128))
    name = db.Column(db.String(64))
    surname = db.Column(db.String(64))
    role = db.Column(db.String(16))
    datetime = db.Column(db.DateTime)
    is_anonymous = False

    def __init__(self, email, name, surname, role):
        self.id = uuid.uuid4().urn
        self.email = email
        self.role = role
        self.name = name
        self.surname = surname
        self.datetime = datetime.datetime.utcnow()

    def __repr__(self):
        return '<User %r>' % self.email

    @property
    def serialize(self):
        return {
            'id': self.id,
            'email': self.email,
            'name': self.name,
            'surname': self.surname,
            'role': self.role
        }

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    def get_urole(self):
        return self.role

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def check_login(email):
        return True if User.query.filter_by(email=email).first() else False


@login.user_loader
def load_user(user_id):
    return User.query.get(user_id)
