from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
from logging.handlers import RotatingFileHandler
import logging

from config import Config

db = SQLAlchemy()
login = LoginManager()
migrate = Migrate()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.static_folder = 'static'

    db.init_app(app)
    login.init_app(app)

    migrate.init_app(app, db)

    from blog.controllers.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    file_handler_error = RotatingFileHandler(app.config['LOG_FILE'], maxBytes=1024 * 1024 * 100, backupCount=20)
    file_handler_error.setLevel(logging.ERROR)
    app.logger.addHandler(file_handler_error)

    return app

from blog import models
