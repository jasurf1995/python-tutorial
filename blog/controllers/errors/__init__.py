from flask import Blueprint

bp = Blueprint('errors', __name__)

from blog.controllers.errors import handlers
