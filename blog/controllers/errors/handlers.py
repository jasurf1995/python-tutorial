from flask import jsonify, current_app
import datetime

from blog.controllers.errors import bp


@bp.app_errorhandler(400)
def bad_request(error):
    response = jsonify({'error': error.description['message']})
    response.status_code = 400
    return response


# Handle unauthorized error
@bp.app_errorhandler(401)
def unauthorized(error):
    response = jsonify({'error': 'User is unauthorized'})
    response.status_code = 401
    return response


# Handle 404 error
@bp.app_errorhandler(404)
def not_found(error):
    response = jsonify({'error': 'page not found'})
    response.status_code = 404
    return response


# Handle server error
@bp.app_errorhandler(500)
def server_error(error):
    current_app.logger.error(datetime.datetime.utcnow().strftime("[%d-%m-%Y %H:%M:%S]"))
    current_app.logger.error("-------------------------------------------------------")
    response = jsonify({'error': 'Something went wrong'})
    response.status_code = 500
    return response
