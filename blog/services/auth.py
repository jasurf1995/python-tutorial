from functools import wraps
from flask import redirect, url_for
from flask_login import current_user

from blog.application import login


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('auth.login'))
            urole = current_user.get_urole()
            if (urole != role) and (role != "ANY"):
                return login.unauthorized()
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper
