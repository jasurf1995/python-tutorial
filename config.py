import os

basedir = os.path.abspath(os.path.dirname(__file__))

APP_FOLDER = os.path.dirname(os.path.abspath(__file__))
EXTERNAL_DIR = os.path.dirname(APP_FOLDER)


class Config(object):
    SERVER_NAME = os.getenv('SERVER_NAME')
    SECRET_KEY = os.getenv('SECRET_KEY')

    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CACHE_TYPE = 'redis'
    CACHE_KEY_PREFIX = 'tutorial'
    CACHE_DEFAULT_TIMEOUT = 1*10*60

    TEMPLATE_FOLDER = os.path.join(APP_FOLDER, 'app/templates')

    LOG_FOLDER = os.path.join(EXTERNAL_DIR, 'logs')
    os.makedirs(LOG_FOLDER, exist_ok=True)

    LOG_FILE = os.path.join(LOG_FOLDER, 'error_log.log')

    DEBUG = False
    TESTING = False
